
Node Save Service
=================

Description
-----------

This module provides a new service method named nodesave.save that uses
node_save() instead of drupal_execute().

Maintainers
-----------

Dick Olsson

Sponsors
--------

NodeOne
